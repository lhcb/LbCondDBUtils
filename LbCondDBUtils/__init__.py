###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
import os


def findDBs():
    '''
    Find Git CondDBs (as by LHCb convention).

    Return a dictionary (name -> path_to_repository)
    '''

    def pathenv(name):
        from os import environ, pathsep
        return environ.get(name, '').split(pathsep)

    from os import listdir
    from re import match
    from os.path import join, isdir
    from itertools import ifilter, imap, chain, count, izip, repeat
    out = {}
    for dirname in ifilter(
            isdir,
            chain(
                pathenv('GITCONDDBPATH'),
                imap(
                    lambda p: join(p, 'git-conddb'),
                    chain(
                        pathenv('CMAKE_PREFIX_PATH'),
                        pathenv('CMTPROJECTPATH'))))):
        for dbname in [
                f.split('.')[0] for f in listdir(dirname)
                if match(r'^[A-Z0-9_]+(\.git)?$', f)
        ]:
            key = next(
                ifilter(
                    lambda k: k not in out,
                    imap('{0[0]} ({0[1]})'.format,
                         izip(repeat(dbname),
                              count(1)))), ) if dbname in out else dbname
            path = join(dirname, dbname)
            if isdir(join(path, '.git')):
                out[key] = path
            elif isdir(path + '.git'):
                out[key] = path + '.git'
    return out


def parse_metadata(s):
    if s is None:
        return None

    from itertools import dropwhile, takewhile, islice
    import yaml

    def not_marker(l):
        return l.strip() != '---'

    def skip_one(iterable):
        return islice(iterable, 1, None)

    return yaml.load(''.join(
        takewhile(not_marker,
                  skip_one(dropwhile(not_marker, s.splitlines(True))))))


def find_parent_tag(commit, cache=None):
    if cache is None:
        cache = {t.commit: t for t in commit.repo.tags}
    else:
        found = cache.get(commit)
        if found:
            return found
    try:
        return next(
            find_parent_tag(parent, cache) for parent in commit.parents)
    except StopIteration:
        return None


class Tag(object):
    def __init__(self, tag):
        self._tagref = tag

    @property
    def name(self):
        return str(self._tagref)

    def __str__(self):
        return self.name

    @property
    def message(self):
        return None if self._tagref.tag is None else self._tagref.tag.message

    def metadata(self):
        return parse_metadata(self.message)

    def datatypes(self):
        return (self.metadata() or {}).get('datatypes', [])

    def base(self):
        tag = find_parent_tag(self._tagref.commit)
        return Tag(tag) if tag else None

    def changes(self, base=None):
        from git import Commit
        base = base or self.base()
        repo = self._tagref.repo
        return Commit.iter_items(repo, '{}..{}'.format(base, self))


class PartitionInfo(object):
    def __init__(self, name):
        from git import Repo
        if os.path.isdir(name):
            self.path = name
            self.name = os.path.basename(self.path)
        else:
            self.path = findDBs()[name]
            self.name = name
        self._repo = Repo(self.path)

    def tags(self, datatype=None):
        for tag in (Tag(t) for t in self._repo.tags):
            if not datatype or datatype in tag.datatypes():
                yield tag
