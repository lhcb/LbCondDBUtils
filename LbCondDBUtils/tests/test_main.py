###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest

import LbCondDBUtils as LCU


@pytest.mark.parametrize("text,expected", [
    ('', None),
    ('---\n', None),
    ('---\n---\n', None),
    ('---\ndatatypes: []\n---\n', {
        'datatypes': []
    }),
    ("---\ndatatypes: [2016, '#2017']\n---\n", {
        'datatypes': [2016, '#2017']
    }),
    ("---\ndatatypes:\n  - 2016\n  - '#2017'\n---\n", {
        'datatypes': [2016, '#2017']
    }),
])
def test_parsing(text, expected):
    assert LCU.parse_metadata(text) == expected
