###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Simple script to make sure metadata (datatypes) in XML release notes match
tag messages in Git.
'''

from __future__ import print_function
from LbCondDBUtils import findDBs, parse_metadata
from LbCondDBUtils.legacy import ReleaseNotes
from git import Repo, GitCommandError
import yaml

rn = ReleaseNotes()
dbs = findDBs()


for partition in rn.partitions().intersection(dbs):
    print('# processing', partition, dbs[partition])
    repo = Repo(dbs[partition])
    for tag in rn.globalTags(partition=partition):
        try:
            rn_metadata = tag.metadata()
            git_tag = repo.tags[tag.name]
            git_metadata = (parse_metadata(git_tag.tag.message)
                            if git_tag.tag else None)
            if rn_metadata != git_metadata:
                print(partition, tag.name,
                      'metadata mismatch (rn->git): %r -> %r' % (rn_metadata,
                                                                 git_metadata))
                # retag
                ref = git_tag.commit
                repo.delete_tag(git_tag)
                repo.create_tag(
                    tag.name,
                    ref=ref,
                    message='---\n{}---\n'.format(yaml.dump(rn_metadata)))

        except IndexError:
            print(partition, tag.name, 'not in git')
        except GitCommandError:
            pass
