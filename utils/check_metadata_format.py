###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Simple script to make sure metadata (datatypes) in XML release notes match
tag messages in Git.
'''

from __future__ import print_function
from LbCondDBUtils import findDBs, parse_metadata
from git import Repo


def sanitize(metadata):
    from copy import deepcopy
    out = deepcopy(metadata)
    if 'datatypes' in out:
        out['datatypes'] = [
            int(t) if hasattr(t, 'isdigit') and t.isdigit() else t
            for t in out['datatypes']]
        # out['datatypes'].sort(reverse=True)
    return out


for name, path in findDBs().items():
    print('# processing', name, path)
    repo = Repo(path)
    for tag in repo.tags:
        metadata = parse_metadata(tag.tag.message) if tag.tag else None
        if not metadata:
            print(name, tag.name, 'metadata missing')
            continue
        expected = sanitize(metadata)
        if expected != metadata:
            print(name, tag.name,
                  'metadata mismatch (found->expected): %r -> %r' % (metadata,
                                                                     expected))
